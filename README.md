# Image Processing - SCC5830. 

# Final Project - Visual analysis of hydrogen bubbles generated by electrolysis of water using Image Processing techniques

Authors:
* Alexis J. Vargas (Usp Number: 11939710)
* Karelia A. Vilca (Usp Number: 11939727)

Semester 1, Year: 2020

# Abstract

The electrolysis of water process generates hydrogen in the form of bubbles that can be used in fuel cells to generate electricity, the analysis of these bubbles could give significant information about the effectiveness of the experiment. This study focuses in analyse a video of the process of electrolysis of water using Image processing to get information of the tiny bubbles frame by frame. 

# Main Objetive

Detect and separate the bubbles in an area of the video, by analyzing frame by frame.
Apply the most appropriate technique for treating small and large bubbles.

# Input Images 

The experiment is based in a video taken of an electrolysis of water process, in which an electrode remains inside a recipient full of water, during the process of electrolysis the generated Hydrogen performs bubbles that goes from the electrode to the surface of the recipient.

The treated video is part of an experiment of the Institute of Physics of São Carlos (IFSC). 

It is formed by 18809 frames and has a resolution of 1920x1080 RGB pixels. Next we show a sample frame.

<img src='../images/full_video.png' width="500"> 

# Pipeline 

1. Pre-processing: Extracting the area of interest.

2. Tiny Bubbles Analysis.

    2.1. Algorithm
    
        2.1.1  Gray-scale Conversion

        2.1.2. Normalization [0 ... 1]

        2.1.3. Edge Detection: Using canny edge detection algorithm.

        2.1.4. Mathematical Morphology: Closing

        2.1.5. Two-Scan Labeling

        2.1.6. Bubble Selection, Threshold
    
    2.2. Process summary  
    
    2.3. Show labeled bubbles with colors
    
    2.4. Experiments and Results
    
    	2.5.1. Executing the algorithm

		2.5.2. Output

		2.5.3. Processing time

		2.5.4. Counting Bubbles 

3. Large Bubbles Analysis.

	3.1. Deep Learning model description

	3.2. Tracking big bubbles

# Implementation and Results 
## 1. Pre-processing
Extracting the area of interest. The first step of the algorithm consist in delimit the region from where the bubbles start appearing until the top of the glass before where the bubbles disappear. This section of video is stored in a separate mp4 file.

Area of interest.

<img src='../images/section.png' width="500"> 

Here we get a frame of the sectional video.

<img src='../images/section0.jpeg' width="300"> 

## 2. Tiny Bubbles Analysis
### 2.1. Algorithm
#### 2.1.1. Gray-scale Conversion
Converting the image into gray-scale is the firststep in order to work in only one channel, this will be done by calculating the mean value of the RGB channels, the obtained value is the intensity of the gray-scale pixel.

#### 2.1.2. Normalization
The normalization is necessary to standardize the image between the values of 0 and 1, in this case the *img_to_float*[2] function gives us this result.

#### 2.1.3. Edge Detection
The Canny algorithm can be used to find the edges in an image, the algorithm get this result by using the first derivative of a Gaussian function and after calculating the average magnitude and the density of the edge length the algorithm perform a threshold to get an accurate result. In this process the canny edge detection algorithm is used to find the edges of all the bubbles, we use an explicit threshold value of 0.8 for the algorithm, we use the function *feature.canny*[2] to compute this part of the process.

#### 2.1.4. Mathematical Morphology
After the edge detection we have the bubbles surrounded  by  lines,  so  the  next  step  is  apply  mathematical  morphology,  we use a rectangle structuring element of size 3x4 and perform the operations of dilation and erosion, these operations together are called “Closing” (we use the *scikit-image*[2]  implementation).  After  this  step  we  have  our  bubbles  all  marked separate from the noise and the background.

#### 2.1.5. Two-Scan Labeling
The labeling process consist in mark each separated object with a different value or label, He et. al.[3] propose an algorithm to get this goal in two steps, first the algorithm iterate over the whole image marking possible groups of pixels connected, this step produce a data structure with the values needed to set a unique label for each group in the list of separated objects. Once  we have the  bubbles marked in  a binary image  separating  the back-ground and the bubbles, the next step is to separate the bubbles labeling each one with a different label, to get this result we use the algorithm proposed in [3] that can perform this task in a linear time which is a good option due to we are going to perform the task for the entire video with 18809 frames.

#### 2.1.6.  Bubble Selection, Threshold
With  the  labels  separated,  we  can  notice  apart  of  the  tiny bubbles, we have some objects labeled that are not of our interest, these are the big bubbles and the small portion of the electrode in the bottom of the image, we can solve this by using a quantity of pixels threshold in the labels, so we only take in count the small labels.

### 2.2. Process summary

Next we can see all the transformations that the frame has had

<img src='../images/process.jpeg' width="700">  

### 2.3. Show labeled bubbles with colors

<img src='../images/colors.png' width="300">  

### 2.4 Complete Algorithm

We synthesize the previous steps in a function label_bubbles

### 2.5 Experiments and Results

We are performing simple experiments to show the behavior of the algorithm, we are going to use 10 consecutive frames of the bubbles video

<img src='../images/experiments.png' width="700"> 

#### 2.5.1. Executing the algorithm

We execute the algorithm in the 10 extracted frames, getting the outputs, execution time, and counting the bubbles

#### 2.5.2. Output

Obtained output after applying our algorithm

<img src='../images/results.png' width="700"> 

#### 2.5.3. Processing time

The processing time is important because this algorithm is going to be applied in a video with 18809 frames.
We estimate the following times:

- Mean of processing times: 192.831 milliseconds
- Estimated total video processing time: 60.449 minutes 

#### 2.5.4. Counting Bubbles
The quantity of bubbles is one of the data of interest in this project, so we left a basic approach counting the bubbles frame by frame, multiple advanced algorithms could be used in order to obtain relevant information for the experiment.

In 10 consecutive frames we obtain the following accounting:

- Frame 1: 77 bubbles
- Frame 2: 76 bubbles
- Frame 3: 77 bubbles
- Frame 4: 73 bubbles
- Frame 5: 73 bubbles
- Frame 6: 75 bubbles
- Frame 7: 74 bubbles
- Frame 8: 78 bubbles
- Frame 9: 76 bubbles
- Frame 10: 71 bubbles

## 3. Large Bubbles Analysis

For the analysis of the large bubbles we had to label each frame with a different color for each bubble, as shown below. To achieve the follow-up of each bubble.

<img src='../images/large.jpeg' width="700"> 

We apply the transfer learning technique by using a pre-trained neural network (ResNet) to predict the trajectory of each bubble, considering the proximity of the center of each box that encloses it through the frames. That is, it could be affirmed that between the frames the center of the bubble will be close, and it will have the tendency to move upwards.

We load the pre-trained data and test with continuous frames of the video to follow a bubble.

<img src='../images/path.jpeg' width="700"> 

We can see how the box behaves properly, following the path of the same bubble.

# Discussion of results
## Tiny Bubbles 
The result of the tiny bubbles analysis is a new data-set conformed by the same quantity of frames from the original video, but containing only the tiny images labeled, this labeled bubbles are harder to track from frame to frame because of their size and their movement in the video.

It was possible to label the bubbles and show a visualization by colors, seeing that most of them are separate.

The analysis of the entire video takes 60,449 minutes, which is still tolerable considering that it has 18,809 frames and a duration of 10 minutes and 27 seconds.

It was also possible to count the number of bubbles that ranges from 71 to 78 in 10 frames, this because several of them disappear, become divided, new ones arise.

## Large Bubbles 
The result of the big bubbles analysis is a new data-set conformed by 23617 possible bubbles tracked, each item of this data-set contains the area and position of the bubble and the path that follows through the video frame by frame.

The path of the bubbles could be marked with a box, this is verified in the course of the frames.

##  Future Work
In both cases we obtain a data-set ready to perform analysis tasks to get relevant information about the electrolysis process, several statistic and image processing algorithms can be used in these data-sets to obtain new data such as the velocity or quantity of the bubbles.
The use of this results can lead into an analysis of the efficiency of different experiments of electrolysis of water, and performing the proposed analysis is possible to gain information about the effectiveness of the process until get a viable way to obtain enough energy to apply the electrolysis as main source of energy of some electronic equipment.
Some of the possible analysis that can be made in the obtained data-sets are distance transformation, area measurement, velocity measurement and image registration.

# Participation of the members

## Alexis J. Vargas

He was in charge of:
- Gathering information.
- Collect the input data (video).
- Investigation of techniques for the treatment of images.
- Labeling of input data for large bubble processing.
- Implementation of the techniques.
- Testing.

## Karelia A. Vilca 

She was in charge of:
- Synthesize the data.
- Investigation of techniques for the treatment of images.
- Improving the implementation of techniques.
- Monitoring of large bubble treatment.
- Testing.
- Document and organize information.

# Demo
In the following notebook we can see the implementation of the process already described, as well as see the results that are shown.
* [Final Report code](/notebooks/Final\ Report.ipynb) contains information, techniques, experiments and results.

# Presentation video
* [video](https://youtu.be/DFfra0VqmzM) Presentation video: https://youtu.be/DFfra0VqmzM

# References

[1] OpenCV, https://opencv.org/

[2] scikit-image: Image processing in Python — scikit-image, https://scikit-image.org/

[3] He, L., Chao, Y., Suzuki, K.: A linear-time two-scan labeling algorithm. In: 2007
IEEE International Conference on Image Processing. vol. 5, pp. V – 241–V – 244
(Sep 2007). https://doi.org/10.1109/ICIP.2007.4379810

## Folders and files:
* [Code](/code) contains code of tiny bubbles process
* [Data](/data) contains information resulting from large bubble analysis
* [Images](/images) contains images used in the demos
* [Notebooks](/notebooks) contains notebooks developed from implementations.
* [Videos](/images) contains input videos to process

* [Partial Report Notebook](/notebooks/Partial\ Report.ipynb) Notebook containing information and documented code corresponding to the partial report
* [Partial Report](/Partial Report.md) Contains the partial report.
* [Final Report Notebook](/notebooks/Final\ Report.ipynb) Notebook containing information, documented code and demo corresponding to the final report
* [Final Report](/README.md) Contains the final report.